VERSION := $(shell echo $(BRANCH_NAME)|sed 's|/|_|g')
VERSION += 666
VERSION := $(word 1, $(VERSION))
VERSION := $(shell echo $(VERSION)-$(BUILD_NUMBER))
PACKAGE := $(shell git config --get remote.origin.url |cut -d: -f2 |sed 's/.git//g')
GIT_TAG := $(shell git tag --points-at HEAD)
MASTER_TAG := $(shell echo master-$(BUILD_NUMBER))
TAG := $(or $(GIT_TAG),$(MASTER_TAG) )
TEST_IMAGE = docker-push.k8s.switchdinlocal/test/$(PACKAGE)
NAME = ecr.aws.switchdinlocal/$(PACKAGE)


build:
	docker build -t $(NAME):$(VERSION) .
	docker build -t $(TEST_IMAGE):$(VERSION) .

build-nocache:
	docker build -t $(NAME):$(VERSION) --no-cache .


tag-latest:
	docker tag $(NAME):$(VERSION) $(NAME):latest

push-test-image:
	docker push $(TEST_IMAGE):$(VERSION) 
push:
	docker push $(NAME):$(VERSION)

push-latest:
	docker push $(NAME):latest

image-dump-and-load:
	docker image save $(NAME):$(VERSION) -o image.tar
	chmod 644 image.tar
	# docker image rm $(NAME):$(VERSION) 
	# docker load  -i image.tar

release: build tag-latest push push-latest

version:
	@echo $(TEST_IMAGE):$(VERSION) Dockerfile > VERSION
	@echo $(NAME):$(VERSION)
