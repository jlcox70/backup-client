FROM  alpine:3
ENV BACKUP_PERIOD=15min
RUN apk --update --no-cache add wget bash file rsync postgresql-client aws-cli ; \
    wget https://dl.min.io/client/mc/release/linux-amd64/mc -O /usr/local/bin/mc; \
    chmod +x /usr/local/bin/mc ; \
    apk del wget ; \
    rm -rf /var/cache/apk/* ; \
    mkdir /files /stage /script

COPY setup.sh /script/
COPY *-backup /script/

CMD ["/script/setup.sh"]