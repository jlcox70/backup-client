#!/bin/bash
k8s_namespace=`cat /var/run/secrets/kubernetes.io/serviceaccount/namespace`
timestamp=`date +%y%m%d-%H:%M`


# Set a default number of copies to keep if unset in the environment
if [ ! $MAX_COPIES ]
then 
        MAX_COPIES=15
fi

if [ ! $BACKUP_USER ]
then
        BACKUP_USER="postgres"
fi

if [  `hostname |awk -F- '{print NF-1}'` -ne 1 ]
then
        short_name=`hostname | awk -F- 'BEGIN {OFS=FS};NF{NF-=2};1'`
else
        short_name=`hostname`
fi

STORAGE="s3://${S3_BUCKET}/${k8s_namespace}/${short_name}"


cd

# test for rsync
if [ `timeout 5 rsync rsync://localhost &>/dev/null ; echo $?` -eq 0 ]
then
        # get the list of rsync repos
        for i in `rsync -Pav rsync://localhost |awk '{print $1","$2}'`
        do

                echo Working on $i    
                repo=`echo $i  |cut -d, -f1`
                repo_path=`echo $i |cut -d, -f2`
                echo REPO = ${repo}     Source Path = ${repo_path}
                mkdir  -p /stage/${repo}/${repo_path}
                FILES_COPIED=`rsync -Pva --stats rsync://localhost/$repo /stage/${repo}/${repo_path} |grep -i transferred: |cut -d: -f2 |sed 's/ //g'`
                echo FILES_COPIED = $FILES_COPIED
                if [ $FILES_COPIED -ne 0 ]
                then
                        cd /stage/$repo
                        tar -jcf /stage/${repo}-${timestamp}.tar .
                        aws s3 cp /stage/${repo}-${timestamp}.tar $STORAGE/${repo}-${timestamp}.tar
                        echo
                        # remove the backup
                        rm /stage/${repo}*.tar
                else
                        echo No update for backups
                fi
                echo
                echo    

        done
fi
# test for postgreSQL
if [ `timeout 5 psql -h localhost -U ${BACKUP_USER} -c 'select 1' &>/dev/null ; echo $?` -eq 0 ]
then
        # individual database backups
        for i in `psql -h 127.0.0.1 -U ${BACKUP_USER} -c "\l" | grep UTF8 |awk '{print $1}' |grep -v template |grep -v postgres`
        do
                pg_dump -h 127.0.0.1 -U ${BACKUP_USER} -d $i -F c -f /stage/${i}-${timestamp}.pg_backup
                aws s3 cp /stage/${i}-${timestamp}.pg_backup $STORAGE/${i}-${timestamp}.pg_backup
                rm -f /stage/${i}-*
                echo
        done
        # Full database backups
        pg_dump -h 127.0.0.1 -U ${BACKUP_USER} -d $i -F c -f /stage/full_system-${timestamp}.pg_backup
        aws s3 cp /stage/full_system-${timestamp}.pg_backup $STORAGE/full_system-${timestamp}.pg_backup

        # roles
        pg_dumpall --roles-only -h 127.0.0.1 -U ${BACKUP_USER} >/stage/roles-${timestamp}.pg_backup
        aws s3 cp /stage/roles-${timestamp}.pg_backup $STORAGE/roles-${timestamp}.pg_backup

        \rm /stage/full_system*
fi