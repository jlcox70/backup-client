#!/bin/bash
k8s_namespace=`cat /var/run/secrets/kubernetes.io/serviceaccount/namespace`
timestamp=`date +%y%m%d-%H:%M`

# Set a default number of copies to keep if unset in the environment
if [ ! $MAX_COPIES ]
then 
        MAX_COPIES=15
fi

if [ ! $BACKUP_USER ]
then
        BACKUP_USER="postgres"
fi

if [  `hostname |awk -F- '{print NF-1}'` -ne 1 ]
then
	short_name=`hostname | awk -F- 'BEGIN {OFS=FS};NF{NF-=2};1'`
else
	short_name=`hostname`
fi	
STORAGE=${k8s_namespace}/${short_name}
mc mb minio/${STORAGE}/archive

cd
mkdir -p .mc
cat > ~/.mc/config.json << EOF
{
        "version": "10",
        "aliases": {
                "minio": {
                        "url": "http://minio-minio.storage-minio.svc.cluster.local:9000/",
                        "accessKey": "${AWS_KEY}",
                        "secretKey": "${AWS_SECRET_KEY}",
                        "api": "s3v4",
                        "path": "auto"
                }
        }
}
EOF

# test for rsync
if [ `timeout 5 rsync rsync://localhost &>/dev/null ; echo $?` -eq 0 ]
then
        # get the list of rsync repos
        for i in `rsync -Pav rsync://localhost |awk '{print $1","$2}'`
        do

                echo Working on $i    
                repo=`echo $i  |cut -d, -f1`
                repo_path=`echo $i |cut -d, -f2`
                echo REPO = ${repo}     Source Path = ${repo_path}
                mkdir  -p /stage/${repo}/${repo_path}
                FILES_COPIED=`rsync -Pva --stats rsync://localhost/$repo /stage/${repo}/${repo_path} |grep -i transferred: |cut -d: -f2 |sed 's/ //g'`
                echo FILES_COPIED = $FILES_COPIED
                if [ $FILES_COPIED -ne 0 ]
                then
                        # archive is there is an update
                        for i in `mc ls minio/${STORAGE}/ |awk '{print $NF}'|grep ${repo}`
                        do
                                echo Archiving $i
                                mc mv minio/${STORAGE}/$i minio/${STORAGE}/archive/  
                        done
                        cd /stage/$repo
                        tar -jcf /stage/${repo}-${timestamp}.tar .
                        mc cp /stage/${repo}-${timestamp}.tar minio/$STORAGE/${repo}-${timestamp}.tar
                        echo
                        #cleanup archives
                        ARCHIVE_COPIES=`mc find minio/${STORAGE}/archive/ |grep ${repo} |wc -l`
                        if [ $ARCHIVE_COPIES -gt $MAX_COPIES ]
                        then
                                EXTRA_COPIES=$((  $ARCHIVE_COPIES - $MAX_COPIES ))
                                for i in `mc find minio/${STORAGE}/archive/ |grep ${repo} | sort |tail -n $EXTRA_COPIES`
                                do
                                        echo remove $i
                                        mc rm $i
                                done        
                        fi
                        # remove the backup
                        rm /stage/${repo}*.tar
                else
                        echo No update for backups
                fi
                echo
                echo    

        done
fi
# test for postgreSQL
if [ `timeout 5 psql -h localhost -U ${BACKUP_USER} -c 'select 1' &>/dev/null ; echo $?` -eq 0 ]
then
        # individual database backups
        for i in `psql -h 127.0.0.1 -U ${BACKUP_USER} -c "\l" | grep UTF8 |awk '{print $1}' |grep -v template |grep -v postgres`
        do
                # archive 
                for k in `mc ls minio/${STORAGE}/ |awk '{print $NF}'|grep ${i}`
                do
                        echo Archiving $k
                        mc mv minio/${STORAGE}/$k minio/${STORAGE}/archive/  
                done
                pg_dump -h 127.0.0.1 -U ${BACKUP_USER} -d $i -F c -f /stage/${i}-${timestamp}.pg_backup
                mc cp /stage/${i}-${timestamp}.pg_backup minio/$STORAGE/${i}-${timestamp}.pg_backup
                #cleanup archives
                ARCHIVE_COPIES=`mc find minio/${STORAGE}/archive/ |grep $i |wc -l`
                
                if [ $ARCHIVE_COPIES -gt $MAX_COPIES ] && [ $ARCHIVE_COPIES -ne 0 ]
                then
                        EXTRA_COPIES=$((  $ARCHIVE_COPIES - $MAX_COPIES ))
                        for j in `mc find minio/${STORAGE}/archive/ |grep ${i} | sort |tail -n $EXTRA_COPIES`
                        do
                                echo remove $j
                                mc rm $j
                        done        
                fi
                rm -f /stage/${i}-*
                echo
        done
        # Full database backups
        # archive 
        for k in `mc ls minio/${STORAGE}/ |awk '{print $NF}'|grep full_system`
        do
                echo Archiving $k
                mc mv minio/${STORAGE}/$k minio/${STORAGE}/archive/  
        done
        pg_dump -h 127.0.0.1 -U ${BACKUP_USER} -d $i -F c -f /stage/full_system-${timestamp}.pg_backup
        mc cp /stage/full_system-${timestamp}.pg_backup minio/$STORAGE/full_system-${timestamp}.pg_backup
        #cleanup archives
        ARCHIVE_COPIES=`mc find minio/${STORAGE}/archive/  |grep full_system |wc -l`
        
        if [ $ARCHIVE_COPIES -gt $MAX_COPIES ] && [ $ARCHIVE_COPIES -ne 0 ]
        then
                EXTRA_COPIES=$((  $ARCHIVE_COPIES - $MAX_COPIES ))
                for j in `mc find minio/${STORAGE}/archive/ |grep full_system | sort |tail -n $EXTRA_COPIES`
                do
                        echo remove $j
                        mc rm $j
                done        
        fi
        \rm /stage/full_system*
fi