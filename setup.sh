#!/bin/bash
if [ ! $BACKUP_PROVIDER ]
then
    export BACKUP_PROVIDER="minio"
fi
case $BACKUP_PERIOD in
    15min)
        ;;
    hourly)
        ;;
    daily)
        ;;
    weekly)
        ;;
    monthly)
        ;;
    *) 
        echo Unknown period setting $BACKUP_PERIOD
        exit
        ;;
esac

echo Setting peroid to $BACKUP_PERIOD for backups
cp /script/${BACKUP_PROVIDER}-backup /etc/periodic/${BACKUP_PERIOD}/
chmod +x /etc/periodic/${BACKUP_PERIOD}/${BACKUP_PROVIDER}-backup
# \rm /etc/periodic/15min/setup.sh
# "crond","-f","-l","4","-d","4"
crond -f -l 4 -d 4